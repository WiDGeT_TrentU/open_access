#load libraries
library(psych)
library(psychTools)
library(gender)
library(lme4)
library(ggplot2)
library(ggpubr)
library(sjPlot)
library(stringr)
library(rsq)
install.packages("genderdata", repos = "http://packages.ropensci.org")
library(genderdata) 
#copy all of the filtered data in from excel (ie. highlight and press copy in excel)
y=read.clipboard(sep="\t",header = T)
#remove columns with missing data
z=subset(y, y$Complete==0)
#predict gender based on first name - not output is alphabetical
gender_temp<-gender(as.character(z$First_Name))
Gender<-gender_temp$gender[match(z$First_Name, gender_temp$name)]
#zz sorted alphabetical before binding gender
zz<-cbind(z,Gender)
df=na.omit(zz)

#Figure 1 / Model 1. Year vs OA rate
#Want year as numeric variable so split; also generate range of years for plot
year1=str_split_fixed(df$Year, "-", 2)
#for boxplot
a=as.numeric(year1[,1])
b=rep("-",length(as.numeric(year1[,1])))
c=as.numeric(year1[,1])+4
d=paste(a,b,c)
e=str_replace_all(d, " ", "")
#linear model and plot
mB_year <- glm(df$No_OA_GC ~ as.numeric(year1[,1])+ offset(log(df$No_pubs_GC)),family = poisson)
summary(mB_year)
rsq(mB_year)
Figure1 <- ggplot(df, aes(x=e, y=Prop_OA)) + geom_boxplot(outlier.shape = NA)+ geom_jitter(shape=16, position=position_jitter(0.2),colour="black") + theme_classic() +ylab("Proportion of OA articles")+xlab("Award Year")
Figure1


#Gender ~ Prop OA / NSERC - Figure 2
Fig2a <- ggplot(df, aes(x=Gender, y=Amount)) + geom_boxplot(outlier.shape = NA)+ geom_jitter(shape=16, position=position_jitter(0.2),colour="black") + theme_classic() +ylab("NSERC Award in $10K CAD")
Fig2b <- ggplot(df, aes(x=Gender, y=Prop_OA)) + geom_boxplot(outlier.shape = NA) + geom_jitter(shape=16, position=position_jitter(0.2),colour="black") + theme_classic() +ylab("Proportion of OA articles")
Figure2 <- ggarrange(Fig2a,Fig2b,
                     labels = c("a", "b"),
                     ncol = 2, nrow = 1)
Figure2

t.test(df$Amount~df$Gender)
t.test(df$Prop_OA~df$Gender)
#----------------------
#Pearson correlation between response variables
cor.test(df$No_OA_GC,df$Prop_OA)
#Mixed models that go with Figure 1

#----------------------
#basic model of OA papers vs amount
amount=df$Amount/10000
m0 <- glm(df$No_OA_GC ~ scale(amount),family = poisson)
summary(m0)

#full model with and without random effect
hindex=df$H.Index_Current
m1 <- glm(df$No_OA_GC ~ scale(amount)*df$Gender +scale(df$H.Index_Current)+as.factor(df$Affiliation_type)+ offset(log(df$No_pubs_GC)),family = poisson)
summary(m1)
rsq(m1)
tab_model(m1)
m1r <- glmer(df$No_OA_GC ~ scale(amount)*df$Gender +scale(hindex)+as.factor(df$Affiliation_type)+offset(log(df$No_pubs_GC))+ (1 | df$Year),family = poisson)
summary(m1r)
rsq(m1r)
tab_model(m1r)